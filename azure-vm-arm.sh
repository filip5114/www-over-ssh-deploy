#!/bin/sh
# echo "Enter the Resource Group name:" &&
# read resourceGroupName &&
# echo "Enter the location (i.e. centralus):" &&
# read location &&
# echo "Enter the project name (used for generating resource names):" &&
# read projectName &&
# echo "Enter the administrator username:" &&
# read username &&

export resourceGroupName="TempResourceGroup" &&
export location="westeurope" &&
export projectName="TempProject" &&
export username="azureuser" &&
export key=`cat ~/.ssh/id_rsa.pub` &&
az group create --name $resourceGroupName --location "$location" &&
az deployment group create --resource-group $resourceGroupName --template-file azure-vm-arm.json --parameters projectName=$projectName adminUsername=$username adminPublicKey="$key" &&
az vm show --resource-group $resourceGroupName --name "$projectName-vm" --show-details --query publicIps --output tsv